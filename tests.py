from services import get_greetings_text, process_link


def test_greetings():
	result = get_greetings_text()
	assert len(result) > 0


def test_process_link_http():
	result = process_link("сообщение со ссылкой http://ya.ru")
	count_of_lines = len(result.split("\n"))
	assert count_of_lines == 2


def test_process_link_https():
	result = process_link("сообщение со ссылкой https://ya.ru")
	count_of_lines = len(result.split("\n"))
	assert count_of_lines == 1
