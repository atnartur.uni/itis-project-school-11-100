def get_greetings_text():
	return "Hello world"


def process_link(message_text):
	result_text = f"Получил ссылку в этом сообщении {message_text}"
	if 'https' not in result_text.lower():
		result_text += "\nПоставьте сертификат на сайте, чтобы подключение к нему было защищено!"
	return result_text