import os
from pprint import pprint

import telebot
from dotenv import load_dotenv

load_dotenv()


from services import get_greetings_text, process_link



TELEGRAM_BOT_TOKEN = os.environ.get("TELEGRAM_BOT_TOKEN")

bot = telebot.TeleBot(TELEGRAM_BOT_TOKEN, parse_mode=None)


@bot.message_handler(commands=['start', 'help'])
def send_welcome(message):
	print('text', message.text)
	print('from user', message.from_user.username)
	bot.reply_to(message, get_greetings_text())


@bot.message_handler(
	func=lambda message: message.text.lower().startswith('http')
)
def print_links(message):
	bot.reply_to(message, process_link(message.text))



print('Bot started')
bot.infinity_polling()
